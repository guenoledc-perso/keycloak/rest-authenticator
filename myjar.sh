#!/bin/sh

# will receive the following params 
# --create --file <name of the jar to be created> -C <folder with the content> .
#  $1        $2   $3                              $4  $5

JARNAME=$3
FOLDER=$5

cd $FOLDER
echo "Manifest-Version: 1.0" > META-INF/MANIFEST.MF
echo "Created-By: 9 (Oracle Corporation)" >> META-INF/MANIFEST.MF

zip -r $JARNAME *

