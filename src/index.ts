import KeycloakScriptPackager from "./deploy/keycloak-packager";
import { RestAuthenticator } from "./rest/rest-authenticator";
import {
  KeycloakRestAuthenticator,
  AuthenticationRequest,
  AuthenticationResponse
} from "./domain/types";

type ProcessFunction = (
  req: AuthenticationRequest
) => Promise<AuthenticationResponse | null>;
const NotImplementedProcessFunction: ProcessFunction = (): Promise<AuthenticationResponse | null> => {
  throw new Error("Not implemented");
};

interface KeycloakRestAuthenticatorClass {
  new (): KeycloakRestAuthenticator;
}
/** Javascript helper to benefit type completion when typescript is not used in implementation */
function createAuthenticator(
  constructor?: (this: KeycloakRestAuthenticator) => KeycloakRestAuthenticator,
  functions?: {
    processNew?: ProcessFunction;
    processInteraction?: ProcessFunction;
    processInterruption?: ProcessFunction;
  }
): KeycloakRestAuthenticatorClass {
  if (!constructor) {
    constructor = function(this) {
      return this;
    };
  }
  constructor.prototype.processNew =
    functions?.processNew || NotImplementedProcessFunction;
  constructor.prototype.processInteraction =
    functions?.processInteraction || NotImplementedProcessFunction;
  constructor.prototype.processInterruption =
    functions?.processInterruption || NotImplementedProcessFunction;
  return (constructor as unknown) as KeycloakRestAuthenticatorClass;
}

export {
  KeycloakScriptPackager,
  RestAuthenticator,
  KeycloakRestAuthenticator,
  AuthenticationRequest,
  AuthenticationResponse,
  createAuthenticator
};
