import fs from "fs";
import path from "path";
import { execSync } from "child_process";
import DebugLib from "debug";
const debug = DebugLib("rest-authenticator:packager");

type AuthenticatorMetadata = {
  name: string;
  fileName: string;
  description: string;
};
const deleteFolderRecursive = function(p: string): void {
  if (fs.existsSync(p)) {
    fs.readdirSync(p).forEach((file, index) => {
      const curPath = path.join(p, file);
      if (fs.lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(p);
  }
};
const _tempdir = ".keycloak-packager";
//const httpRegex = /(https?:\/\/(?:[[:alnum:]]+\.)?[[:alnum:]]+\.com)(\/\S*)/;

export type KeycloakScriptPackagerConfig = {
  /**  path to the keycloak `deployments` folder where the jar file should be placed
   * so keycloak can pick it up. Typically it is `/opt/jboss/keycloak/standalone/deployments`.
   * If not provided it is set to the current folder */
  keycloakDeploymentLocation?: string;
  /** name of the jar file that should be created by the packager.
   * if not specified the name of the current folder will be used.
   */
  deployedJarFileName?: string;
  /** Mandatory, root url of express http service as it can be reached from keycloak server.
   * This is used to create the full endpoint of the authenticator to tell keycloak how to reach it.
   */
  keycloakAccessibleBaseUrl: string;
  /** name of the program to create the jar. This is defaulted to the `jar` string to assume that
   * the jar program is installed in the target environment and available in the PATH.
   */
  jarExecutable?: string;
  /** location of a temporary directory where the jar content will be constructed.
   * By default it will use the current process location */
  tempDirectory?: string;
};

function checkUrlValidity(url: string): string | undefined {
  try {
    const u = new URL(url);
    return u.href;
  } catch (error) {
    return undefined;
  }
}

/** Class to package the handler into a keycloack jar following the specifications at https://www.keycloak.org/docs/latest/server_development/#create-a-jar-with-the-scripts-to-deploy */
export default class KeycloakScriptPackager {
  private targetLocation: string;
  private jarName: string;
  private authenticators: AuthenticatorMetadata[];
  private jarExecutable: string;
  private targetBaseUrl: string;
  private tempDir: string;
  /** create an instance with the specific configuration */
  constructor(config: KeycloakScriptPackagerConfig) {
    this.authenticators = [];
    this.targetLocation = config.keycloakDeploymentLocation || ".";
    this.jarName =
      config.deployedJarFileName || path.basename(process.cwd()) + ".jar";
    this.jarExecutable = config.jarExecutable || "jar";
    this.tempDir = config.tempDirectory || process.cwd();

    if (
      !config.keycloakAccessibleBaseUrl ||
      !checkUrlValidity(config.keycloakAccessibleBaseUrl)
    ) {
      throw new TypeError(
        `config.keycloakAccessibleBaseUrl is mandatory and expected to be a valid url: ${config.keycloakAccessibleBaseUrl}`
      );
    }
    this.targetBaseUrl = config.keycloakAccessibleBaseUrl;
  }

  private getKeycloakScriptJson(): any {
    const res = {
      authenticators: this.authenticators,
      policies: [],
      mappers: []
    };
    return res;
  }
  private tempdirName(): string {
    return path.join(this.tempDir, _tempdir);
  }
  private createTempDir(): void {
    if (!fs.existsSync(this.tempdirName())) {
      fs.mkdirSync(this.tempdirName());
    }
  }
  private deleteTempDir(): void {
    if (fs.existsSync(this.tempdirName())) {
      deleteFolderRecursive(this.tempdirName());
    }
  }
  private createMetaInf(): void {
    const folder = path.join(this.tempdirName(), "META-INF");
    deleteFolderRecursive(folder);
    fs.mkdirSync(folder);
    fs.writeFileSync(
      path.join(folder, "keycloak-scripts.json"),
      JSON.stringify(this.getKeycloakScriptJson(), null, 2),
      "UTF8"
    );
  }
  private createAuthenticatorFile(meta: AuthenticatorMetadata): void {
    const folder = path.join(this.tempdirName(), path.dirname(meta.fileName));
    deleteFolderRecursive(folder);
    fs.mkdirSync(folder);
    fs.copyFileSync(
      path.join(__dirname, "rest-authenticator.js"),
      path.join(folder, path.basename(meta.fileName))
    );
  }
  private makeJar(): void {
    const cmd = `${this.jarExecutable} --create --file ${path.join(
      path.resolve(this.targetLocation),
      this.jarName
    )} -C ${this.tempdirName()} .`;
    debug("Creating jar cmd:%s, output: %s", cmd);
    const out = execSync(cmd, { timeout: 2000 });
    debug("Creating jar cmd:%s, output: %s", cmd, out.toString("utf8"));
  }

  /**
   * Adds a new endpoint into the package to be created with the `make()` method
   * @param restPath path to be appended to the `config.keycloakAccessibleBaseUrl` so a target url is defined in keycloak
   * @param description describe that endpoint. it is defaulted with a description if not specified. In anycase a bug in keycloak makes this description not available in the admin interface.
   */
  addRestAuthenticator(restPath: string, description?: string): void {
    // authenticator name is a varchar(36) in keycloak db and the string will be created as 'script-'+fineName
    // therefore the filename should be max 36 - 'script-'.length
    // but the file should be a '.js' file so we should construct max 36 - 'script-'.length - '.js'.length
    function makeFileName(p: string): string {
      const max = 36 - "script-".length - ".js".length;
      const file = path.join(p, "authenticator-rest").substring(0, max);
      return file + ".js";
    }
    const url = this.targetBaseUrl + restPath;
    if (!checkUrlValidity(url)) {
      throw new TypeError(
        `Requested path does not construct a valid url: ${url}`
      );
    }
    this.authenticators.push({
      name: this.targetBaseUrl + restPath,
      fileName: makeFileName(restPath),
      // we put the description but as of today the description is not available in keycloak due to a limitation in keycloak code
      description:
        description || "Transmit the authentication execution to the url"
    });
  }

  /**
   * Construct the jar file with the endpoints declared with `addRestAuthenticator()`
   * and place it in the `config.keycloakDeploymentLocation` or current path
   */
  make(): void {
    try {
      this.createTempDir();
      this.createMetaInf();
      this.authenticators.forEach(auth => {
        this.createAuthenticatorFile(auth);
      });
      this.makeJar();
    } finally {
      this.deleteTempDir();
    }
  }
}
