/*
* Template for JavaScript based authenticator's.
* See org.keycloak.authentication.authenticators.browser.ScriptBasedAuthenticatorFactory
*/
// see https://wiki.openjdk.java.net/display/Nashorn/

// import needed java classes into javascript class
var AuthenticationFlowError = Java.type("org.keycloak.authentication.AuthenticationFlowError");
var JSON2 = Java.type("org.keycloak.util.JsonSerialization");
var HttpClients = Java.type("org.apache.http.impl.client.HttpClients");
var HttpPost = Java.type("org.apache.http.client.methods.HttpPost");
var StringEntity = Java.type("org.apache.http.entity.StringEntity");
var EntityUtils = Java.type("org.apache.http.util.EntityUtils");
var AuthenticationManager = Java.type("org.keycloak.services.managers.AuthenticationManager");
var URI = Java.type("java.net.URI");
var Response = Java.type("javax.ws.rs.core.Response");

// const
var restAuthenticatorNote = "restAuthenticator";
var restAuthenticatorGroupName = "RestAuthenticator";

// LOG is globally defined

function checkUserPresent(user, context) {
    if(!user) {
        LOG.warn("RestAuthenticator: The user must be known and defined");
        context.failure(AuthenticationFlowError.CLIENT_CREDENTIALS_SETUP_REQUIRED );
        return false;
    }
    LOG.info("USER :"+user.username+" ("+user.firstName+" "+user.lastName+")");
    return true;
}

// returns a UserModel instance
function createOrUpdateUser(context, userInfo) {
    var user = null;
    // ensure userInfo is defined with the minimum info
    if(!userInfo || !(userInfo.username || userInfo.email)) return null
    // test if we should use the email as key
    if (userInfo.email && !context.getRealm().isDuplicateEmailsAllowed()) {
        user = context
          .getSession()
          .users()
          .getUserByEmail(userInfo.email, context.getRealm());
    } 
    else 
    // test if the key should be the username
    if(userInfo.username) {
        user = context
          .getSession()
          .users()
          .getUserByUsername(userInfo.username, context.getRealm());
    }
    // here we have either a user or none. if none, lets create one new user
    if(!user) {
        user = context
          .getSession()
          .users()
          .addUser(context.getRealm(), userInfo.username || userInfo.email);
    }
    // now we have a created user; Lets set the fields
    if(user) {
        if(userInfo.email) user.setEmail(userInfo.email)
        if(userInfo.emailVerified) user.setEmailVerified(userInfo.emailVerified)
        if(userInfo.firstName) user.setFirstName(userInfo.firstName);
        if(userInfo.lastName) user.setLastName(userInfo.lastName);
        if(userInfo.enabled == false) user.setEnabled(false);
        else user.setEnabled(true);
    }
    return user
}

function getRestAuthenticatorUrl(realm, context) {
    return script.getName();
}

function getNoteId(context) {
    return context.getExecution().getId()
}

function updateAuthNoteState(context, authenticationSession) {
    var modified = false;
    var authNote = JSON.parse(authenticationSession.getAuthNote(getNoteId(context)) || "{}");
    if (authNote.callCount) {
        authNote.callCount = authNote.callCount + 1;
        modified = true;
    }
    
    if(modified) {
        authenticationSession.setAuthNote(getNoteId(context), JSON.stringify(authNote));
    }
}

function createBody(context, realm, user, authenticationSession, httpRequest, mode) {
    
    var body = {
        authenticator: context.getExecution().getAuthenticator(),
        execution: context.getExecution().getId(),
        mode: mode,
        realm: {
            id: realm.id,
            name: realm.name,
            attributes: JSON.parse(JSON2.writeValueAsString(realm.getAttributes()))
        },
        authSession: {
            clientNotes: JSON.parse(JSON2.writeValueAsString(authenticationSession.getClientNotes())),
            clientScopes: JSON.parse(JSON2.writeValueAsString(authenticationSession.getClientScopes())),
            authNote: JSON.parse(authenticationSession.getAuthNote(getNoteId(context)) || "{}"),
            executionStatus: JSON.parse(JSON2.writeValueAsString(authenticationSession.getExecutionStatus())),
            userSessionNotes: JSON.parse(JSON2.writeValueAsString(authenticationSession.getUserSessionNotes())),
            tabId: authenticationSession.getTabId(),
            action: authenticationSession.getAction(),
            protocol: authenticationSession.getProtocol(),
        },
        client: {
            id: authenticationSession.getClient().getId(),
            clientId: authenticationSession.getClient().getClientId(),
            name: authenticationSession.getClient().getName(),
            baseUrl: authenticationSession.getClient().getBaseUrl(),
            description: authenticationSession.getClient().getDescription(),
            attributes: JSON.parse(JSON2.writeValueAsString(authenticationSession.getClient().getAttributes()))
        },
        httpRequest: {
            uri: httpRequest.getUri().getRequestUri().toString(),
            uriPath: httpRequest.getUri().getPath(),
            uriQueryParams: JSON.parse(JSON2.writeValueAsString(httpRequest.getUri().getQueryParameters(true))),
            headers: JSON.parse(JSON2.writeValueAsString(httpRequest.getHttpHeaders())),
            formParams: JSON.parse(JSON2.writeValueAsString(httpRequest.getDecodedFormParameters()))
        },
        connection: {
            address: context.getConnection().getRemoteAddr(),
            host: context.getConnection().getRemoteHost(),
            port: context.getConnection().getRemotePort()
        }
    };
    // try populating the user
    if(user) {
        body.user = {
            id: user.id,
            username: user.username,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            createdTimestamp: user.createdTimestamp,
            emailVerified: user.isEmailVerified(),
            enabled: user.isEnabled(),
            attributes: JSON.parse(JSON2.writeValueAsString(user.getAttributes()))
        }
    }
    // try getting the info of an existing session with the keycloak cookie
    var authResult = AuthenticationManager.authenticateIdentityCookie(context.getSession(), context.getRealm(), true);
    if (authResult) {
        body.userSession = {
            authMethod: authResult.getSession().getAuthMethod(),
            loginUsername: authResult.getSession().getLoginUsername(),
            notes: JSON.parse(JSON2.writeValueAsString(authResult.getSession().getNotes())),
            isRememberMe: authResult.getSession().isRememberMe()
        };
    }
    else {
        //LOG.info("AUTH RESULT IS NULL");
    }
    return body;
}


function postToRestService(restAuthenticatorUrl, body) {
    var httpClient = HttpClients.createDefault();
    var post = new HttpPost(restAuthenticatorUrl);
    var res = null;
    try { // call the rest server and get the response in synchronous mode
        LOG.info("RestAuthenticator:" + restAuthenticatorUrl);
        LOG.info("RestAuthenticator authSession:" + JSON.stringify(body.authSession));
        post.setHeader("Content-Type", "application/json");
        post.setEntity(new StringEntity(JSON.stringify(body), "UTF-8"));
        var response = httpClient.execute(post);
        LOG.info("RestAuthenticator status:"
            + response.getStatusLine().getStatusCode());
        LOG.info("RestAuthenticator response content-type:"
            + response.getEntity().getContentType().getValue());
        var data = EntityUtils.toString(response.getEntity());
        LOG.info("RestAuthenticator response data:" + data);
        if (response.getStatusLine().getStatusCode() == 200) {
            res = JSON.parse(data);
        }
        else {
            LOG.error("FAILED with status code != 200");
            res = null;
        }
        response.close();
    }
    catch (error) {
        LOG.error("FAILED with error:" + error.toString());
    }
    return res;
}

function processRestResponse(res, context, user, authenticationSession, httpRequest) {
    // handle response from the server
    // first try to see if a user should be created
    if( !user && res.user ) {
        LOG.info("Creating user " + res.user.username || res.user.email);
        user = createOrUpdateUser(context, res.user);
        context.setUser(user);
    }
    
    // set the session and user attributes as defined by the response
    if( res.authNote ) {
        authenticationSession.setAuthNote(getNoteId(context), JSON.stringify(res.authNote));
    }

    if(res.userAttributes) {
        for(var att in res.userAttributes) {
            if(user) {
                user.setSingleAttribute(att, res.userAttributes[att]);
            } else {
                LOG.warn("Cannot set the user attribute '"+att+"' since the user does not yet exists!")
            }
        }
    }

    if(res.userSessionNotes) {
        for(var note in res.userSessionNotes) {
            authenticationSession.setUserSessionNote(note, res.userSessionNotes[note]);
        }
    }

    // handle response effect on the authentication flow
    if(res.fork) context.fork();

    // The control failed and no form is to be displayed : fail login
    var failureError = null;
    if (res.failure) {
        if(res.failure=="invalidCredentials") {
            failureError = AuthenticationFlowError.INVALID_CREDENTIALS;
        } else if(res.failure=="unknownUser") {
            failureError = AuthenticationFlowError.INVALID_USER;
        } else {
            failureError = AuthenticationFlowError.CREDENTIAL_SETUP_REQUIRED;
        }
    } 
    // declare a form (ie a challenge)
    var challenge = null;
    if(res.challenge && res.challenge.redirectUri) {
        var uri = new URI(res.challenge.redirectUri);
        var challenge = Response.seeOther(uri).build();
        LOG.info("Redirect attempt " + challenge.getLocation().toString() )
        
    } else if(res.challenge) {
        // the response has a challenge object, interpret it and create the form
        var formProvider = context.form();
        if(res.challenge.message) {
            formProvider.setInfo(res.challenge.message, []);
        }
        if(res.challenge.messageHeader) {
            formProvider.setAttribute("messageHeader",res.challenge.messageHeader);
        }
            
        if( res.challenge.formData ) {
            for(var key in res.challenge.formData) {
                formProvider.setAttribute(key, res.challenge.formData[key]);
            }
        }

        switch (res.challenge.formName || "InfoPage") {
            case "LoginTotp":
                challenge = formProvider.createLoginTotp();
                break;
            case "InfoPage":
                challenge = formProvider.createInfoPage();
                break;
            default:
                challenge = formProvider.createForm(res.challenge.formName);
                break;
        }
    } 

    if(failureError && !challenge) {
        // error occurred and no form to display
        context.failure(failureError)
        //context.cancelLogin();
        return;
    } 
    if(failureError && challenge) {
        // error occured and a form to display
        context.failureChallenge(failureError, challenge);
        return;
    }
    if(challenge) {
        // there is no error and a form to display
        context.challenge(challenge);
    } else {
        // if no error or challenge required, it is a success login for this authenticator
        context.success();
    }
}

/**
 * An example authenticate function.
 *
 * The following variables are available for convenience:
 * user - current user {@see org.keycloak.models.UserModel}
 * realm - current realm {@see org.keycloak.models.RealmModel}
 * session - current KeycloakSession {@see org.keycloak.models.KeycloakSession}
 * httpRequest - current HttpRequest {@see org.jboss.resteasy.spi.HttpRequest}
 * script - current script {@see org.keycloak.models.ScriptModel}
 * authenticationSession - current authentication session {@see org.keycloak.sessions.AuthenticationSessionModel}
 * LOG - current logger {@see org.jboss.logging.Logger}
 *
 * You one can extract current http request headers via:
 * httpRequest.getHttpHeaders().getHeaderString("Forwarded")
 *
 * @param context {@see org.keycloak.authentication.AuthenticationFlowContext}
 */
function handleCall(context, mode) {
    LOG.info("#####################################################################################");
    LOG.info("SCRIPT id="+script.getId()+"; name="+script.getName()+"; description="+script.getDescription()+"; realmId="+script.getRealmId());
    //if(!checkUserPresent(user, context)) return;
    
    var restAuthenticatorUrl = getRestAuthenticatorUrl(realm, context);
    if(!restAuthenticatorUrl) return;
    
    updateAuthNoteState(context, authenticationSession);
    
    var body = createBody(context, realm, user, authenticationSession, httpRequest, mode);
    
    var res = postToRestService(restAuthenticatorUrl, body);
    
    if(res === null) { // the response has not been set properly : fail with technical error
        context.cancelLogin();
        //context.failure(AuthenticationFlowError.DISPLAY_NOT_SUPPORTED );
    } else {
        processRestResponse(res, context, user, authenticationSession, httpRequest);
    }
    
}

function authenticate(context) {
    handleCall(context, "authenticate");
}


function action(context) {
    handleCall(context, "action");
}