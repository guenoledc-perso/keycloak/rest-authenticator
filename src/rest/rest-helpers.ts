import { Request, Response } from "express";
import {
  AuthenticationRequest,
  RealmModel,
  UserModel,
  UserSessionModel,
  AuthSessionModel,
  ClientModel,
  HttpRequestModel,
  ConnectionModel,
  AuthenticationRequestMode
} from "../domain/types";

export class InvalidRequest extends Error {
  constructor(message?: string) {
    super(message);
  }
}

function ensure<T>(field: any, fieldName = "unspecified"): T {
  if (!field) {
    throw new InvalidRequest(
      `Field ${fieldName} is not defined in the request`
    );
  }
  return field as T;
}

export function getAuthenticationRequest(req: Request): AuthenticationRequest {
  if (!req.body) {
    throw new InvalidRequest("The request has no body");
  }
  return {
    authenticator: ensure<string>(req.body.authenticator, "authenticator"),
    execution: ensure<string>(req.body.execution, "execution"),
    mode: ensure<AuthenticationRequestMode>(req.body.mode, "mode"),
    realm: ensure<RealmModel>(req.body.realm, "realm"),
    user: req.body.user ? (req.body.user as UserModel) : undefined,
    userSession: req.body.userSession
      ? (req.body.userSession as UserSessionModel)
      : undefined,
    authSession: ensure<AuthSessionModel>(req.body.authSession, "authSession"),
    client: ensure<ClientModel>(req.body.client, "client"),
    httpRequest: ensure<HttpRequestModel>(req.body.httpRequest, "httpRequest"),
    connection: ensure<ConnectionModel>(req.body.connection, "connection")
  };
}
