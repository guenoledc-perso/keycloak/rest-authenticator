/** Information representing the realm in which the authentication is currently happening */
export type RealmModel = {
  /** unique id of the realm (not the clsid but the literal)*/
  id: string;
  /** name of the realm */
  name: string;
  /** list of all attributes of the realm */
  attributes: { [key: string]: string };
};

/** Information representing the user info to populate a new user authenticated by the authenticator */
export type UserInfo = {
  /** mandatory if email is missing */
  username?: string;
  /** mandatory if username is missing */
  email?: string;
  firstName?: string;
  lastName?: string;
  emailVerified?: boolean;
  enabled?: boolean;
};

/** Information representing the user for which the authentication is taking place */
export type UserModel = {
  id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  createdTimestamp: number;
  emailVerified: boolean;
  enabled: boolean;
  /** The attributes the user has in keycloak */
  attributes: { [key: string]: string[] };
};

export type ExecutionStatus =
  | "ATTEMPTED"
  | "CHALLENGED"
  | "FAILED"
  | "SETUP_REQUIRED"
  | "SKIPPED"
  | "SUCCESS";

export type AuthNoteModel = {
  /** Reserved. Used to identify if an ongoing call is being interrupted */
  callCount?: number;
  [key: string]: any;
};

export type AuthSessionModel = {
  /** a dictionnary with the parameter of the authentication request from the end user. Typically the oidc parameter of the /auth endpoint. */
  clientNotes: { [key: string]: string };
  /** to be documented */
  clientScopes: string[];
  /** disctionnary of value where you can store informations between two calls to remain stateless. This will be segregated from any other authentication prcess and other authenticators. */
  authNote: AuthNoteModel;
  /** list of the passed execution statuses. Not easy to interpret at this stage */
  executionStatus: { [key: string]: ExecutionStatus };
  /** in memory attribute for the user's session that have been set by any authenticator in keycloak or in your program during this authentication. They can be used in the mappers and unlike the user's attributes they are not persisted. */
  userSessionNotes: { [key: string]: string };
  /** keycloak identification of the browser tab interacting with it */
  tabId: string;
  /** Should always be `AUTHENTICATE` */
  action: string;
  /** the protocol used for the client  */
  protocol: "openid-connect" | "saml" | string;
};

export type ClientModel = {
  id: string;
  clientId: string;
  name: string;
  baseUrl: string;
  description: string;
  attributes: { [key: string]: string };
};

export type HttpRequestCookieModel = {
  name: string;
  value: string;
  version: number;
};
export type HttpRequestMediaType = {
  type: string;
  subtype: string;
  parameters: { [key: string]: string };
  wildcardType: boolean;
  wildcardSubtype: boolean;
};
export type HttpRequestHeadersModel = {
  requestHeaders: { [key: string]: string[] };
  cookies: { [key: string]: HttpRequestCookieModel };
  length: number;
  mutableHeaders: { [key: string]: string[] };
  mutableCookies: { [key: string]: HttpRequestCookieModel };
  mediaType?: HttpRequestMediaType;
  acceptableMediaTypes: HttpRequestMediaType[];
  acceptableLanguages: string[];
};

export type HttpRequestModel = {
  uri: string;
  uriPath: string;
  uriQueryParams: { [key: string]: string[] };
  headers: { [key: string]: string[] };
  formParams: { [key: string]: string[] };
};

export type ConnectionModel = {
  /** ip address of the end-user browser if transmitted accross the network, or the last network device communicating with keycloak. */
  address: string;
  host: string;
  port: number;
};

export type UserSessionModel = {
  /** how the session was established : typically `openid-connect` */
  authMethod: string;
  /** the keycloak user name of the session */
  loginUsername: string;
  /** dictionarry of fields kept and set by keycloak for the session. This is where you will find the `response.userSessionNotes` when set on a previous authentication process */
  notes: { [key: string]: string };
  /** Boolean to see if the user will be remembered after a logout */
  isRememberMe: boolean;
};

export type AuthenticationRequestMode = "authenticate" | "action";

/** The information passed from keycloak to the authenticator to initiate an authentication */
export interface AuthenticationRequest {
  /** the unique id of the authenticator provider. With script it is 'script-' + the script name. */
  authenticator: string;
  /** the clsid of the current execution. AuthNotes are stored in keycloak against this id. This is guaranted to be unique foe each execution but common accross multiple calls for the same authentication */
  execution: string;
  /** Defines if the request comes from the initialization of the authentication ('authenticate') or from the subsequent interactions with the forms (challenges) presented ('action') */
  mode: AuthenticationRequestMode;
  /** Realm information in which the authentication is taking place */
  realm: RealmModel;
  /** Client information for which the authentiction is in progress */
  client: ClientModel;
  /** User information for which the authentication is taking place. It can be missing if the authenticator is placed before authenticators that identify the user. */
  user?: UserModel;
  /** Existing user session information. It is missing if the user has no current session. This is deduced from the cookie. */
  userSession?: UserSessionModel;
  /** Information on the current authentication process. This is passed accorss multiple calls and authenticator and can be used to save data accross calls */
  authSession: AuthSessionModel;
  /** the http request received by keycloak from the browser to trigger this authentication interaction. In particular it will carry the forms data posted by the user in clear. */
  httpRequest: HttpRequestModel;
  /** the http connection of the end user's browser. This is where you can find the end-user's ip address */
  connection: ConnectionModel;
}

export type FormNameType = "InfoPage" | "LoginTotp" | string;

/** Defines how keycloak should display a form */
export type FormChallengeModel = {
  /** sets the `message.summary` attribute in the freemarker template*/
  message?: string;
  /** sets the `messageHeader` attribute in the freemarker template*/
  messageHeader?: string;
  /** sets any json structure attributes in the freemarker template*/
  formData?: { [key: string]: any };
  /** sets the name of the freemarker template. You can use one of the predefined or you should specify the full name of the form. For instance `"login-password.ftl"`. If not specified it will be interpreted as `"InfoPage"` */
  formName?: FormNameType;
};
/** Defines the url to get keycloak redirect to it */
export type RedirectChallengeModel = {
  /** set the challenge to be a redirection to a new url. This takes precedence over the other fields */
  redirectUri?: string;
};

/** The response to be provided to keycloak after an authentication interaction */
export interface AuthenticationResponse {
  /** user information to define the user authenticated. Will be ignored if the user is already identified in the request. */
  user?: UserInfo;
  /** dictionnary of fields that will be returned on subsequent calls for the same authentication execution. The `callCount` field is reserved and will be forced. */
  authNote?: AuthNoteModel;
  /** dictionnary of attributes that you want keycloak to set for the user. If the user is not defined, warning will be generated in keycloak log and they will not be set. */
  userAttributes?: { [key: string]: string };
  /** dictionnary of in memory attributes you want keycloak to set to the user session. These will also be available in the token mappers */
  userSessionNotes?: { [key: string]: string };
  /** set this to true to force keycloak to fork the authentication process. To be further explained. */
  fork?: boolean;
  /** set this to one of the value to trigger a failure of the authentication in keycloak. Can be used in conjuction with the `challenge` to display a form in case of error. */
  failure?: "invalidCredentials" | "unknownUser" | "otherCause";
  /** set the form (or challenge) information that keycloak must display as a response to this authentication execution. It must refer to an existing form name in the current keycloak template. It can be a simple redirect url if what is required is a redirection to another page */
  challenge?: FormChallengeModel | RedirectChallengeModel;
}

export interface KeycloakRestAuthenticator {
  /**
   * This method is called when `mode` is "authenticate", meaning at the start of the execution of the authentication.
   * It must return a response with either a failure, a challenge or nothing (meaning success)
   * If null is returned then it means a failure.
   * @param req the information provided by keycloak script authenticator
   * @returns AuthenticationResponse or null
   */
  processNew(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null>;
  /**
   * This method is called when `mode` is "action", meaning after a normal interaction with the responded form (or challenge) of `processNew()`.
   * For long processing (eg blocking) think to enable end user interruption with a button to call again the same url.
   * @param req req the information provided by keycloak script authenticator
   * @returns AuthenticationResponse or null
   */
  processInteraction(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null>;
  /**
   * This method is called when `mode` is "action" and when `processInteraction` is currently in progress.
   * To interrupt the current process, the `response.fork` will be set to `true` so that keycloak will ignore the final response of the concurrent process.
   * Such processing should not be blocking.
   * @param req req the information provided by keycloak script authenticator
   * @returns AuthenticationResponse or null
   */
  processInterruption(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null>;
}
